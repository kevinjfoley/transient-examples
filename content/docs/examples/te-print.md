+++
title = "te-print"
author = ["Kevin Foley"]
lastmod = 2021-03-21T15:19:58-04:00
draft = false
weight = 2201
+++

{{< highlight emacs-lisp >}}
(defun te--opt (opt args)
  "Find and return OPT in ARGS"
  (when (string-match
	 (rx (group
	      (regexp opt)
	      (minimal-match (zero-or-more anychar)))
	     (or "-" line-end))
	 args)
    (substring (match-string 1 args) (1+ (string-match "=\\| " (match-string 1 args))))))

(defun te--arg (arg args)
  "Find and return ARG in ARGS"
  (if (string-match
       (rx (group (regexp arg))
	   (or " " line-end))
	args)
      t
    nil))

(defun te-print (&optional args)
  "Returns the contents of a file using ARGS.

ARGS should be a string with arguments and options similar to
those used for a command line utility.

The follow are valid ARGS:

template        - The template file to use.  Otherwise default template is \"{{ greeting }} {{ name }}!\"
--var           - Values to assign to var's in template.  Ex: --var greeting=hi
-v, --verbose   - Verbosity"
  (let* ((args (or args ""))
	 (template-file (car (split-string args "-\\| ")))
	 (template
	  (when (length> template-file 0)
	    (with-temp-buffer (insert-file-contents template-file)
			      (buffer-string))))
	 (verbose (te--arg "-v\\|--verbose" args))
	 ;; TODO handle multiple --var's
	 (vars (mapcar
		(lambda (arg)
		  (split-string arg "=" t " "))
		(when-let (var (te--opt "--var" args))
		  (split-string
		   var
		   " "))))

	 ;; Default template and vars
	 (vars (and (not template)
		    (append vars
			    '(("greeting" "Hello")
			      ("name" "world")))))
	 (template (or template
		       "{{ greeting }} {{ name }}!")))
    (concat
     (and verbose "Printing file contents...\n")
     (dolist (var vars template)
       (setq template (string-replace
		       (format "{{ %s }}" (car var))
		       (cadr var)
		       template))))))
{{< /highlight >}}
