+++
title = "Your First Transient"
author = ["Kevin Foley"]
lastmod = 2021-03-21T15:17:08-04:00
draft = false
weight = 2001
+++

## Introduction {#introduction}

`transient` is often used to provide a friendly interface to CLI's.  In this tutorial we will walk through creating such an interface.


## The "CLI" {#the-cli}

For the purposes of this tutorial, we'll use an Emacs function, `te-print`, meant to mimic a CLI.

You can find the code for `te-print` [here]({{< relref "te-print" >}}).  Don't worry about the code that makes up `te-print`, it's not important for this tutorial and is just intended to be used for demonstrative purposes.  Simply evaluate it in your Emacs session.

`te-print` essentially renders a template by replacing variables with the values you specify using the `var` option.

By default, it uses the template `{{ greeting }} {{ name }}!` with the `var`'s `greeting=Hello` and `name=world`.  Therefore calling the following from the shell:

{{< highlight shell >}}
te-print
te-print --var greeting=Hey
{{< /highlight >}}

would return:

```text
Hello world!
Hey world!
```

An example of it being called from Emacs is:

{{< highlight emacs-lisp >}}
(te-print)
(te-print "--var greeting=Hey")
{{< /highlight >}}

```text
Hello world!
Hey world!
```

`te-print` has some other features as well but we'll discuss those later.


## Creating the Transient {#creating-the-transient}

Next we'll create a transient to call `te-print`.

First, we'll need an elisp command to call `te-print`, since for the purposes of this tutorial, we're pretending `te-print` is a CLI.

{{< highlight emacs-lisp >}}
(defun te-print-run (&optional args)
  "Run the te-print CLI and display results."
  (interactive)
  (with-current-buffer
      ;; Get or create buffer to hold results
      (get-buffer-create "*te-print results*")
    (special-mode)
    (let ((inhibit-read-only t))
      (erase-buffer)
      (insert (te-print (or args "")))))
  (pop-to-buffer "*te-print results*"))
{{< /highlight >}}

Now we can create a temporary file with our template and call `te-print-run` which should display a buffer with our rendered template.

{{< highlight emacs-lisp >}}
(te-print-run)
{{< /highlight >}}

Now that we have an elisp command, we can create our transient.

{{< highlight emacs-lisp >}}
(define-transient-command te-print-transient
  "Call te-print CLI."
  ["Actions"
   ("r" "Run" te-print-run)])
{{< /highlight >}}

Now try calling `M-x te-print-transient`, then pressing `r`.  You should get a buffer with `Hello world!` in it.

Congratulations!  You've created a CLI transient.  You're probably thinking it doesn't do much, but that's okay, we'll add additional capabilities throughout this tutorial.


## Handling an Argument {#handling-an-argument}


## Handling an Option {#handling-an-option}
